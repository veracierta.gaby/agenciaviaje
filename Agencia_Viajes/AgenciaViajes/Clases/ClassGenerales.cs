﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgenciaViajes.Clases
{
    public class ClassGenerales
    {
        [Serializable]
        public partial class TipoViaje
        {

        }




        [Serializable]
        public partial class Viajeros
        {

        }

        [Serializable]
        public partial class Viajes
        {
            private int _CantidaViajes;
            public int CantidadSeleccionada
            {
                get { return _CantidaViajes; }
                set { _CantidaViajes = value; }
            }

            private decimal _MontoTotal;
            public decimal Total
            { 
                get { return _MontoTotal; }
                set { _MontoTotal = value; }
            }

            private int _NumeroPlazas;

            public int Get_NumeroPlazas()
            { return Get_NumeroPlazas(); }
            public void Set_NumeroPlazas(int value)
            { Set_NumeroPlazas(value); }

           

            private bool _IdTipoViaje;

            public bool Get_IdTipoViaje()
            { return Get_IdTipoViaje(); }
            public void Set_IdTipoViaje(bool value)
            { Set_IdTipoViaje(value); }
        }

      
    }
}
