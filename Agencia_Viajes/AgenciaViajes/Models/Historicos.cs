﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgenciaViajes.Models
{
    public class Historicos
    {
        public int IdHistorico { get; set; }

        public int IdViajero { get; set; }

        public int IdViaje { get; set; }

        public int IdTipoPago { get; set; }
        
        public int TipoViaje { get; set; }

        //public virtual Viajeros Viajeros { get; set; }
        //public virtual MetodoPago MetodoPago { get; set; }
    }
}
