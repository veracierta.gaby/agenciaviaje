﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgenciaViajes.Models
{
    public class MetodoPago
    {
        public int IdPago { get; set; }

        public decimal MontoTotal { get; set; }

        public string TipoPago { get; set; }


        public virtual Viajes GetViajes { get; set; }
        public virtual Viajeros GetViajeros { get; set; }
        public virtual ICollection<Historicos> Historicoes { get; set; }
    }
}
