﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static AgenciaViajes.Clases.ClassGenerales;

namespace AgenciaViajes.Models
{

    public partial class TipoViaje
    {
        public TipoViaje()
      
        {
            Viajes = new HashSet<Viajes>();
        }
    

    public int IdTipoViaje { get; set; }

        private string tipoViaje;

        public string GetTipoViaje()
        {
            return tipoViaje;
        }

        public void SetTipoViaje(string value)
        {
            tipoViaje = value;
        }

        public bool Descripcion { get; set; }
    public bool MetodoPago { get; set; }

    public virtual ICollection<Viajes> Viajese { get; set; }
        public HashSet<Viajes> Viajes { get; }
    }
}
