﻿using AgenciaViajes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgenciaViajes.Models
{
    public class Viajeros
    {
        public int IdViajero { get; set; }

        public string Nombre { get; set; }

        public int CI { get; set; }

        public string Direccion { get; set; }

        public string Telefono { get; set; }

        public virtual ICollection<Viajes> Viajeses { get; set; }

        public virtual ICollection<Historicos> GetHistoricos { get; set; }
    }
}
