﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgenciaViajes.Models
{
    public class Viajes
    {
        public int IdViaje { get; set; }
        
        public int IdViajero { get; set; }

        public string Origen { get; set; }
        public string Destino { get; set; }

        public System.DateTime FechaIda { get; set; }

        public System.DateTime Fecha_Retorno { get; set; }

        public int CantidaViajes { get; set; }

        public int IdPago { get; set; }

        public int IdTipoViaje { get; set;  }
    }
}
